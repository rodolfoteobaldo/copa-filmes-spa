import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CopaSelecaoFilmesComponent } from './copa/copa-selecao-filmes/copa-selecao-filmes.component';
import { CopaResultadoComponent } from './copa/copa-resultado/copa-resultado.component';

const routes: Routes = [
  { path: '', redirectTo: 'copa-selecao-filmes', pathMatch: 'full' },
  { path: 'copa-selecao-filmes', component: CopaSelecaoFilmesComponent },
  { path: 'copa-resultado', component: CopaResultadoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
