import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MatCheckboxModule, MatButtonModule } from '@angular/material';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabecalhoComponent } from './shared/cabecalho/cabecalho.component';
import { CopaResultadoComponent } from './copa/copa-resultado/copa-resultado.component';
import { CopaSelecaoFilmesComponent } from './copa/copa-selecao-filmes/copa-selecao-filmes.component';
import { CopaService } from './copa/services/copa.service';
import { HttpClientModule } from '@angular/common/http';
import { FilmesCopaComponent } from './copa/copa-selecao-filmes/filmes-copa/filmes-copa.component';
import { PosicaoCampeonatoComponent } from './copa/copa-resultado/posicao-campeonato/posicao-campeonato.component';
import { PartidaComponent } from './copa/copa-resultado/partida/partida.component';

@NgModule({
  declarations: [
    AppComponent,
    CabecalhoComponent,
    CopaResultadoComponent,
    CopaSelecaoFilmesComponent,
    FilmesCopaComponent,
    PosicaoCampeonatoComponent,
    PartidaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCheckboxModule,
    MatButtonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    CopaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
