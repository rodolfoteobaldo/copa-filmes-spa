import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-cabecalho',
  templateUrl: './cabecalho.component.html',
  styleUrls: ['./cabecalho.component.css']
})
export class CabecalhoComponent {

  @Input() tituloCampeonato = 'CAMPEONATO DE FILMES';
  @Input() tituloTela = 'Fase de seleção';
  @Input() tituloTexto = 'Selecione 8 filmes que você deseja que entrem na competição e ' +
                         'depois pressione o botão Gerar Meu Campeonato para prosseguir.';

  constructor() { }
}
