/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By, BrowserModule } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CopaResultadoComponent } from './copa-resultado.component';
import { CabecalhoComponent } from 'src/app/shared/cabecalho/cabecalho.component';
import { PosicaoCampeonatoComponent } from './posicao-campeonato/posicao-campeonato.component';
import { PartidaComponent } from './partida/partida.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MatCheckboxModule, MatButtonModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CopaSelecaoFilmesComponent } from '../copa-selecao-filmes/copa-selecao-filmes.component';
import { FilmesCopaComponent } from '../copa-selecao-filmes/filmes-copa/filmes-copa.component';
import { AppComponent } from 'src/app/app.component';
import { ResultadoCopa } from '../models/resultado-copa';
import { Filme } from '../models/filme';
import { Partida } from '../models/partida';

class MockResultadoCopa {
  obterDado() {
    const filmeCampeao: Filme = {id: 'tt3606756', titulo: 'Os Incríveis 2', ano: 2018, nota: 8.5};
    const filmeViceCampeao: Filme = {id: 'tt4881806', titulo: 'Jurassic World: Reino Ameaçado', ano: 2018, nota: 6.7};
    const partida: Partida = {rodada: 1, vencedor: filmeCampeao, perdedor: filmeViceCampeao};
    const resultadoCopa: ResultadoCopa = {campeao: filmeCampeao, viceCampeao: filmeViceCampeao, partidas: [partida]};
    return resultadoCopa;
  }
}

describe('CopaResultadoComponent', () => {
  let component: CopaResultadoComponent;
  let fixture: ComponentFixture<CopaResultadoComponent>;
  let resultadoCopa: ResultadoCopa;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        CabecalhoComponent,
        CopaResultadoComponent,
        CopaSelecaoFilmesComponent,
        FilmesCopaComponent,
        PosicaoCampeonatoComponent,
        PartidaComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        MatCheckboxModule,
        MatButtonModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot()
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopaResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    resultadoCopa = new MockResultadoCopa().obterDado();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
