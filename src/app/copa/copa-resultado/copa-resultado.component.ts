import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResultadoCopa } from '../models/resultado-copa';
import { Posicao } from '../models/posicao';
import { Filme } from '../models/filme';
import { CopaService } from '../services/copa.service';
import { Partida } from '../models/partida';

@Component({
  selector: 'app-copa-resultado',
  templateUrl: './copa-resultado.component.html',
  styleUrls: ['./copa-resultado.component.css']
})
export class CopaResultadoComponent implements OnInit {

  tituloTela = 'Resultado Final';
  tituloTexto = 'Veja o resultado final do campeonato de filmes de forma simples e rápida';
  resultadoCopa: ResultadoCopa;
  resultadoPosicao: Posicao[] = [];
  filmesSelecionados: Filme[];
  partidas: Partida[] = [];

  constructor(private router: Router) { }

  ngOnInit() {
    this.resultadoCopa = history.state;
    this.partidas = this.resultadoCopa.partidas;
    this.gerarPosicao();
  }

  gerarPosicao() {
    if (this.resultadoCopa === null || this.resultadoCopa === undefined) {
      return;
    }

    const campeao: Posicao = {
      posicao: 1,
      filme: this.resultadoCopa.campeao
    };
    this.resultadoPosicao.push(campeao);
    const vicecampeao: Posicao = {
      posicao: 2,
      filme: this.resultadoCopa.viceCampeao
    };
    this.resultadoPosicao.push(vicecampeao);
  }

  verificaExisteCampeao() {
    if (this.resultadoCopa.campeao === undefined || this.resultadoCopa.campeao === null) {
      this.router.navigateByUrl('/copa-selecao-filmes');
    }
  }
}
