import { Component, OnInit, Input } from '@angular/core';
import { Partida } from '../../models/partida';

@Component({
  selector: 'app-partida',
  templateUrl: './partida.component.html',
  styleUrls: ['./partida.component.css']
})
export class PartidaComponent implements OnInit {
  @Input() partidas: Partida[];

  exibir: boolean = false ;

  constructor() { }

  ngOnInit() {
    if (this.partidas === null || this.partidas === undefined) {
      this.partidas = [];
    }
  }

  mostrarPartidas()  {
    this.exibir = !this.exibir;
  }
}
