import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-posicao-campeonato',
  templateUrl: './posicao-campeonato.component.html',
  styleUrls: ['./posicao-campeonato.component.css']
})
export class PosicaoCampeonatoComponent {
  @Input() posicao = '';
  @Input() descricao = '';

  constructor() { }
}
