import { Component, OnInit } from '@angular/core';
import { CopaService } from '../services/copa.service';
import { Filme } from '../models/filme';
import { Router, NavigationExtras } from '@angular/router';
import { ResultadoCopa } from '../models/resultado-copa';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-copa-selecao-filmes',
  templateUrl: './copa-selecao-filmes.component.html',
  styleUrls: ['./copa-selecao-filmes.component.css']
})
export class CopaSelecaoFilmesComponent implements OnInit {

  public filmes: Filme[] = [];
  public filmesSelecionados: Filme[] = [];

  constructor(private copaService: CopaService, private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.copaService.obterFilmes()
      .subscribe(
        filmes => {
          console.log(filmes);
          this.filmes = filmes;
        },
        error => console.error(error));
  }

  gerarCampeonato() {
    if (!this.validaQuantidadeFilmes()) {
      return;
    }

    this.copaService.gerarCampeonato(this.filmesSelecionados)
      .subscribe(
        result => { this.onCopaFilmeComplete(result); },
        fail => { this.onError(fail); }
      );
  }

  validaQuantidadeFilmes(): boolean {
    if (this.filmesSelecionados.length !== 8) {
      this.toastr.warning('Para gerar o campeonato é necessário 8 filmes');
      return false;
    }

    return true;
  }

  onCopaFilmeComplete(resultadoCopa: ResultadoCopa): void {
    const navigationExtras: NavigationExtras = {
      state: resultadoCopa
    };
    this.router.navigateByUrl('/copa-resultado', navigationExtras);
  }

  onError(fail) {
    console.error(fail);
    this.toastr.error('Ocorreu algum problema no servidor');
  }

  filmeSelecionadoRetorno(filmeSelecionado) {
    if (filmeSelecionado.checked) {
      this.filmesSelecionados.push(filmeSelecionado.filme);
    } else {
      this.filmesSelecionados.splice(this.obterPosicaoFilmeLista(filmeSelecionado.filme), 1);
    }
  }

  obterPosicaoFilmeLista(filme: Filme) {
    return this.filmesSelecionados.map(item => item.id).indexOf(filme.id);
  }
}
