import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Filme } from '../../models/filme';

@Component({
  selector: 'app-filmes-copa',
  templateUrl: './filmes-copa.component.html',
  styleUrls: ['./filmes-copa.component.css']
})
export class FilmesCopaComponent {
  @Input() filmes: Filme[];
  @Output() filmeSelecionado = new EventEmitter();

  filmesTeste: Filme[] = [];

  constructor() { }

  selecionarFilme(checked, filme: Filme) {
    this.filmeSelecionado.emit({checked, filme});
  }
}
