/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By, BrowserModule } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CopaSelecaoFilmesComponent } from './copa-selecao-filmes.component';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { CabecalhoComponent } from 'src/app/shared/cabecalho/cabecalho.component';
import { FilmesCopaComponent } from './filmes-copa/filmes-copa.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { CopaResultadoComponent } from '../copa-resultado/copa-resultado.component';
import { PosicaoCampeonatoComponent } from '../copa-resultado/posicao-campeonato/posicao-campeonato.component';
import { AppComponent } from 'src/app/app.component';
import { PartidaComponent } from '../copa-resultado/partida/partida.component';

describe('CopaSelecaoFilmesComponent', () => {
  let component: CopaSelecaoFilmesComponent;
  let fixture: ComponentFixture<CopaSelecaoFilmesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        CabecalhoComponent,
        CopaResultadoComponent,
        CopaSelecaoFilmesComponent,
        FilmesCopaComponent,
        PosicaoCampeonatoComponent,
        PartidaComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        HttpClientModule,
        ToastrModule.forRoot()
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopaSelecaoFilmesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
