import { Filme } from './filme';

export class Partida {
    rodada: number;
    vencedor: Filme;
    perdedor: Filme;
}
