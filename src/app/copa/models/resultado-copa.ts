import { Filme } from './filme';
import { Partida } from './partida';

export class ResultadoCopa {
    campeao: Filme;
    viceCampeao: Filme;
    partidas: Partida[];
}
