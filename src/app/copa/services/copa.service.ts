import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { ServiceBase } from 'src/app/services/service.base';
import { ResultadoCopa } from '../models/resultado-copa';
import { Filme } from '../models/filme';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CopaService extends ServiceBase {
  constructor(private http: HttpClient, private router: Router) {
    super();
  }

  gerarCampeonato(filmes: Filme[]): Observable<ResultadoCopa> {
    return this.http
      .post(this.UrlServiceBase + 'copa-filmes', filmes, this.ObterHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError)
      );
  }

  obterFilmes(): Observable<Filme[]> {
    return this.http
      .get<Filme[]>(this.UrlServiceBase + 'filmes');
  }
}
