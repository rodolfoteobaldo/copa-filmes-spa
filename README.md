# SPA da aplicação Copa Filmes

SPA da aplicação Copa Filmes

## O que está implementado:

- Angular versão 8.1.1
- Angular Material
- ngx-toastr

## Como utilizar:
- Será necesário instalar o [NPM](https://nodejs.org/en/).
- Clonar o projeto com o comando:
```sh
$ git clone https://gitlab.com/rodolfoteobaldo/copa-filmes-spa.git
```

- Executar a aplicação para o Desenvolvimento com o comando:
```sh
$ npm install
$ ng serve
```

> Acessar aplicação na url padrão `http://localhost:4200/`

- Executar os teste com o comando:
```sh
$ ng test
```

### Observação:

- O SPA está configurado para executar o serviço na url padrão da API que é `https://localhost:5001/`